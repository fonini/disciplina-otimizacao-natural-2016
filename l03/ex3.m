%
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
%

J = @(x) sum( ((x+1).*x.*(x-1)).^2 + x.^2/10 );
R = @(x) x + 0.1*randn(size(x));

T0 = 0.1;
N = 1e6; % número de iterações dentro de cada metropolis
M = 1023; % número de realizações do metropolis
T_plot = [1 3 31 1023];
T = T0 ./ log2(1+(1:M));

x0 = randn(20,1);

[x_plot, js_plot, x_min_plot, x_min, J_min] = ...
    simanneal(x0, N, J, R, T, T_plot);

L = N/10;
limits = [0,5];
bins = 300;
dj = diff(limits)/bins;
t = (limits(1)+dj/2):dj:(limits(2)-dj/2);

T = T(T_plot);
for k = 1:length(T_plot);
    figure;
    hist(js_plot((N-L+1):N, k),t);
    xlabel('Custo');
    title(sprintf('T = %f  --  custo médio = %f', T(k), mean(js_plot(:,k))));

    figure;
    plot(js_plot(:,k));
    ylabel('Custo');
    title(sprintf('T = %f  --  custo mínimo = %f', T(k), J(x_min_plot(:,k))));

    figure;
    plot(x_plot{k}');
    title(sprintf('T = %f', T(k)));
    grid;
end
