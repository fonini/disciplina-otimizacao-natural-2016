%
% Adaptado de: 070403 gabriel@pads.ufrj.br
%              http://www.pads.ufrj.br/~gabriel/cpe723/Exemplo1.m
%
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
%

P = 5; % número de partículas
lambda = 0.5; % multiplicador de lagrange
J = @(x) J_eg1(x, lambda);
R = @(x) x + 0.1*randn(size(x));

T0 = 1;
N = 1e5; % número de iterações dentro de cada metropolis
M = 10; % número de realizações do metropolis
T = T0 ./ log2(1+(1:M));
T_plot = [1 3 6 10];

x0 = randn(2*P,1);
x_plot  = zeros(size(x0,1), length(T_plot));
js_plot = zeros(N,          length(T_plot));
x_min_plot = x_plot;

w = waitbar(0);
x_min = [];
J_min = Inf;
k_plot = 1;
for k=1:length(T);
    waitbar((k-1)/length(T), w, sprintf('Temperatura #%d: %f', k-1, T(k)));
    [X, js, this_x_min, this_J_min] = metropolis(x0, N, J, R, T(k));
    if this_J_min < J_min
        x_min = this_x_min;
        J_min = this_J_min;
    end
    x0 = X(:,end);
    if find(T_plot == k)
        x_plot(:,k_plot) = x0;
        js_plot(:,k_plot) = js';
        x_min_plot(:,k_plot) = x_min;
        k_plot = k_plot + 1;
    end
end;
delete(w);

L=N/10;
limits = [0,20];
bins = 200;
dj = diff(limits)/bins;
t = (limits(1)+dj/2):dj:(limits(2)-dj/2);

T = T(T_plot);
for k = 1:length(T_plot);
    figure;
    hist(js_plot((N-L+1):N, k),t);
    xlabel('Custo');
    title(sprintf('T = %f  --  custo médio = %f', T(k), mean(js_plot(:,k))));

    figure;
    x_typ = reshape(x_plot(:,k), 2,[]);
    plot(x_typ(1,:), x_typ(2,:), 'o');
    title(sprintf('T = %f  --  estado típico  --  custo = %f', ...
          T(k), J(x_plot(:,k))));
    grid;
    hold on;
    plot(0,0, 'rx');
    axis([-3 3 -3 3]);

    figure;
    x_m = reshape(x_min_plot(:,k), 2,[]);
    plot(x_m(1,:), x_m(2,:), 'o');
    title(sprintf('T = %f  --  estado barato  --  custo = %f', ...
          T(k), J(x_min_plot(:,k))));
    grid;
    hold on;
    plot(0,0, 'rx');
    axis([-3 3 -3 3]);
end
