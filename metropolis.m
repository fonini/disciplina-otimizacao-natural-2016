function [X, js, x_min, J_min] = metropolis(x0, N, J, R, T)
%METROPOLIS Algoritmo de Metropolis.
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
% Inputs:
%   x0       vetor coluna Mx1 contendo o estado inicial.
%   N        quantidade de iterações.
%   J        function handle para uma função de um argumento que retorna a
%            energia de cada estado, e.g., J(x0) é a energia do estado
%            inicial.
%   R        function handle para uma função que retorne um novo estado
%            gerado aleatoriamente a partir de um estado atual.
%   T        temperatura, já em unidades de J.
% Output:
%   X        matriz MxN, onde a n-ésima coluna é o estado no tempo n.
%   js       vetor 1xN contendo as energias dos estados X gerados.
%   x_min    estado (vetor Mx1) com menor energia encontrado.
%   J_min    energia (escalar 1x1) do estado x_min.

X = zeros(size(x0,1), N);
js = zeros(1,N);

X(:,1) = x0;
js(1) = J(x0);
x_min = x0;
J_min = js(1);

for k=2:N
    x_hat = R(X(:,k-1));
    J_hat = J(x_hat);
    delta_J = J_hat - js(k-1);
    q = exp(-delta_J/T);
    r = rand;
    if r > q
        X(:,k) = X(:,k-1);
        js(k) = js(k-1);
    else
        X(:,k) = x_hat;
        js(k) = J_hat;
        if J_hat < J_min
            x_min = x_hat;
            J_min = J_hat;
        end
    end
end

end
