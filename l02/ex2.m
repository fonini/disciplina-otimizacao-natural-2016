%
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
%

J = [0.5  0.2  0.3  0.1  0.4];
R = @(x) 1+mod( x-1 + sign(randn) , 5);

T0 = 0.1;
N = 1e3; % número de iterações dentro de cada metropolis
M = 10; % número de realizações do metropolis
T_plot = 1:M;
T = T0 ./ log2(1+(1:M));

x0 = 1;

[x_plot, js_plot, x_min_plot, x_min, J_min] = ...
    simanneal(x0, N, J, R, T, T_plot);

L = N;
t = [0.1  0.2  0.3  0.4  0.5];

T = T(T_plot);
for k = 1:length(T_plot);
    figure;
    hist(js_plot((N-L+1):N, k), t);
    hold on;
    v_boltz = exp(-t/T(k));
    v_boltz = v_boltz/sum(v_boltz);
    plot(t, L*v_boltz, 'r');
    xlabel('Custo');
    title(sprintf('T = %f  --  custo médio = %f', ...
        T(k), mean(js_plot((N-L+1):N,k))));

    fprintf('T = %f\n    último estado = %d\n    energia = %f\n', ...
        T(k), x_plot{k}(end), J(x_plot{k}(end)));

    fprintf('    estado ótimo = %d\n    energia = %f\n', ...
        x_min_plot(k), J(x_min_plot(k)));

    figure;
    plot(x_plot{k}');
    title(sprintf('T = %f', T(k)));
    grid;
end
