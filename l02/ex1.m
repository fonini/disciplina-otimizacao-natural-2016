%
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
%

M = [2 1 1;1 2 1;1 1 2]/4;
csM = cumsum(M,2);

N=100;
T=4;

X=zeros(N,T);
X(:,1) = floor(1+size(M,1)*rand(N,1));
for t = 2:T
    % A variável aleatória X(t|t-1) tem uma CDF, dada pela soma cumulativa
    % da linha de M referente ao estado X(t-1) dado:
    CDF_t = csM(X(:,t-1),:);
    % Para gerar uma amostra X(t) da distribuição dada por essa CDF, basta
    % gerar um número aleatório entre 0 e 1, e contar quantos termos da
    % CDF estão acima desse número:
    r = rand(N,1);
    r = repmat(r, [1 size(M,1)]);
    X(:,t) = 1 + sum(r > CDF_t, 2);
end

for t=1:T
    figure;
    hist(X(:,t), 1:size(M,1));
    title(sprintf('t = %d', t-1));
end
