#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Evolutionary Programming
"""

import random
import copy

import EC

# TODO: fazer uma opção para usar o "Yao's improved fast EP algorithm":
#       cada pai gera dois filhos: um a partir de uma distribuição de
#       Cauchy, e outro usando uma Gaussiana.

def EvolutionaryProgramming(L, U, mu, *,
                            gene_mutation_rate=None,
                            apply_mutation=None,
                            apply_gene_crossover=None,
                            select_surv=None,
                            rrobin_rounds=None,
                            use_elitism=False,
                            **kwargs):

    assert len(L) == len(U)
    crlen = len(L)

    if gene_mutation_rate is None:
        gene_mutation_rate = 1/crlen

    if apply_mutation is None:
        apply_mutation = lambda self: self.mutt_uelp()

    if apply_gene_crossover is None:
        def apply_gene_crossover(alleles):
            alpha = random.expovariate(2)
            gamma = random.uniform(-alpha, 1 + alpha)
            return gamma*alleles[0] + (1-gamma)*alleles[1]

    if select_surv is None:
        if rrobin_rounds is None:
            select_surv = lambda self, chil, elit: self.select_rrtour(
                chil, elitism=elit)
        else:
            select_surv = lambda self, chil, elit: self.select_rrtour(
                chil, q=rrobin_rounds, elitism=elit)

    FI, FP = EC.FloatEC(L, U, **kwargs)

    class EPIndividual(FI):
        def __init__(self, chromossome=None,
                     **kwas):
            super().__init__(chromossome=chromossome,
                             gene_mutation_rate=gene_mutation_rate,
                             **kwas)
        def mutate(self):
            apply_mutation(self)
            self.update_fitness()

    class EPPopulation(FP):
        def __init__(self, Typ=EPIndividual):
            super().__init__(mu, Typ, recombination_rate=0.0)
        def get_parents(self):
            return [([m], 1) for m in self.members]
        def make_reproduce(self, parents, m):
            # len(parents) == m == 1
            child = copy.deepcopy(parents[0])
            child.age = 0
            return [child]
        def survivor_selection(self, children):
            select_surv(self, children, use_elitism)

    return EPIndividual, EPPopulation
