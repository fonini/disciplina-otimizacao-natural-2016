#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Evolution Strategies
"""

import random
import copy

import EC

def EvolutionStrategy(L, U, mu, lambd, *,
                      gene_mutation_rate=None,
                      recombination_rate=1.0,
                      apply_mutation=None,
                      global_recombination=True,
                      chromossome_recomb='discrete',
                      parameters_recomb='intermediary',
                      apply_gene_crossover=None,
                      select_surv=None,
                      use_elitism=True,
                      **kwargs):

    assert len(L) == len(U)
    crlen = len(L)

    if gene_mutation_rate is None:
        gene_mutation_rate = 1/crlen

    if apply_mutation is None:
        apply_mutation = lambda self: self.mutt_uelp()

    if apply_gene_crossover is None:
        def apply_gene_crossover(alleles):
            alpha = random.expovariate(2)
            gamma = random.uniform(-alpha, 1 + alpha)
            return gamma*alleles[0] + (1-gamma)*alleles[1]

    if select_surv is None:
        select_surv = lambda self, chil, elit: self.select_mulambd(chil, elit)

    FI, FP = EC.FloatEC(L, U, **kwargs)

    class ESIndividual(FI):
        def __init__(self, chromossome=None,
                     **kwas):
            super().__init__(chromossome=chromossome,
                             gene_mutation_rate=gene_mutation_rate,
                             **kwas)
        def mutate(self):
            apply_mutation(self)
            self.update_fitness()

    class ESPopulation(FP):
        def __init__(self, Typ=ESIndividual):
            super().__init__(mu, Typ, recombination_rate)
        def get_parents(self):
            if global_recombination:
                return [(self.members[:], lambd)]
            else:
                return [(random.sample(self.members, 2), 1)
                        for _ in range(lambd)]
        def make_reproduce(self, parents, m):
            children = []
            for _ in range(m):
                if random.random() >= self.recombination_rate:
                    p = random.choice(parents)
                    c = p.chromossome[:]
                    params = {kw+'_init': copy.deepcopy(getattr(p, kw))
                            for kw in p.evol_parameters}
                else:
                    if chromossome_recomb == 'discrete':
                        c = []
                        for k in range(crlen):
                            allele = random.choice(parents).chromossome[k]
                            c.append(allele)
                    else:
                        c = []
                        for k in range(crlen):
                            a0 = random.choice(parents).chromossome[k]
                            a1 = random.choice(parents).chromossome[k]
                            c.append(apply_gene_crossover([a0, a1]))
                    params = {}
                    for kw in self.Typ.evol_parameters:
                        if self.Typ.evol_parameters[kw] == 'scalar':
                            if parameters_recomb == 'discrete':
                                p = random.choice(parents)
                                new_param = getattr(p, kw)
                            else:
                                p0 = random.choice(parents)
                                param0 = getattr(p0, kw)
                                p1 = random.choice(parents)
                                param1 = getattr(p1, kw)
                                new_param = apply_gene_crossover(
                                    [param0, param1])
                        elif self.Typ.evol_parameters[kw] == 'vec':
                            veclen = len(getattr(parents[0], kw))
                            new_param = []
                            for l in range(veclen):
                                if parameters_recomb == 'discrete':
                                    p = random.choice(parents)
                                    new_param.append(getattr(p, kw)[l])
                                else:
                                    p0 = random.choice(parents)
                                    param0 = getattr(p0, kw)[l]
                                    p1 = random.choice(parents)
                                    param1 = getattr(p1, kw)[l]
                                    new_param.append(apply_gene_crossover(
                                        [param0, param1]))
                        params[kw+'_init'] = new_param
                children.append(self.Typ(c, **params))
            return children
        def survivor_selection(self, children):
            select_surv(self, children, use_elitism)

    return ESIndividual, ESPopulation
