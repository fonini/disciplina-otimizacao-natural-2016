#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Evolutionary Computing

Problema das oito rainhas.
Espaço dos genótipos: Grupo simétrico S_8, ou seja, o espaço de todas as
    permutações de 8 elementos: [0,1,2,3,4,5,6,7], ..., [7,6,5,4,3,2,1,0].
Espaço dos fenótipos: Espaço de todas as configurações do tabuleiro com 8
    rainhas que satisfazem as restrições de xeque horizontal e vertical (ou
    seja, as rainhas só se colocam em xeque nas diagonais)
"""

import EC
import random

N = 8 # oito rainhas

PI, PP = EC.PermutationEC(N)

class Configuração(PI):

    def __init__(self, cromossomo=None, taxa_mutação=0.8):
        if cromossomo is None:
            cromossomo = random.sample(range(N), N)
        super().__init__(cromossomo, taxa_mutação)

    def __repr__(self):
        return ("<Configuração do tabuleiro com genótipo {}, fitness {}, "
                "idade {}>").format(
            '-'.join(str(d) for d in self.chromossome),
            self.fitness, self.age
        )

    def phenotype(self):
        """Lista de linhas do tabuleiro.

        Para fazer um tabuleiro bonitinho:
            print("\n".join(["".join(linha)
                             for linha in self.phenotype()]))
        """
        linha     = ["·"      for _ in range(N)]
        tabuleiro = [linha[:] for _ in range(N)]
        for k in range(N):
            tabuleiro[k][self.chromossome[k]] = 'Q'
        return tabuleiro

    def update_fitness(self):
        # Número total de pares de rainhas: T=N*(N-1)/2
        T = N * (N-1) // 2
        # Número de pares em xeque: C
        C = 0
        for k in range(N):
            for l in range(k+1, N):
                # se a rainha da linha ‘k’ estiver em xeque com a rainha da
                # linha ‘l’, incrementa C.
                if l-k == abs(self.chromossome[l]-self.chromossome[k]):
                    C += 1
        # Fitness: F=T-C
        self.fitness = T - C

    def mutate(self):
        # Escolhe duas posições no cromossomo, e troca.
        if N < 2:
            return
        if random.random() >= self.mutation_rate:
            return
        self.mutt_swap()
        # TODO: fazer o update fitness na mão, aproveitando que a gente sabe
        #       exatamente o que mudou
        self.update_fitness()

class População8Q(PP):

    def __init__(self, n, taxa_recombinação=1.0):
        super().__init__(n, Configuração, taxa_recombinação)

    def __repr__(self):
        fmt = "<População8Q com size={}, best_genotype={}, geração {}a.>"
        return fmt.format(self.size, self.best_genotype, self.generation)

    def get_parents(self, best=2, rnd=5):
        """Best 2 out of random 5"""
        assert rnd <= self.size
        pais = random.sample(self.members, rnd)
        pais.sort()
        del pais[:-best]
        random.shuffle(pais)
        return [(pais, 2)]

    def make_reproduce(self, pais, m):
        # TODO: para fazer m>2, a gente poderia escolher m-1 crossover points,
        #       e aí cada filho seria a junção de todos os m pais.
        assert m == 2
        if random.random() >= self.recombination_rate:
            return [Configuração(p.chromossome[:]) for p in pais]
        crianças = self.recomb_cut_crossfill([p.chromossome for p in pais])
        return [Configuração(c[:]) for c in crianças]

    def survivor_selection(self, crianças):
        self.members += crianças
        self.sort()
        del self.members[:len(crianças)]
