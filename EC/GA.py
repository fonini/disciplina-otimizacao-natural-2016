#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Genetic Algorithms
"""

import random

import EC

def SimpleGeneticAlgorithm(crlen, gene_mutation_rate=None,
                           recombination_rate=None):

    if gene_mutation_rate is None:
        gene_mutation_rate = 1/crlen

    if recombination_rate is None:
        recombination_rate = 0.7

    BI, BP = EC.BinaryEC(crlen)

    class SGAIndividual(BI):
        def __init__(self, chromossome=None):
            super().__init__(chromossome, gene_mutation_rate)

    class SGAPopulation(BP):
        def __init__(self, n, Typ=SGAIndividual):
            assert n % 2 == 0
            super().__init__(n, Typ, recombination_rate)
        def get_parents(self):
            parents = []
            lambd = self.size
            prob_distribution = self.parentsel_fps()
            pool = self.getpool_roulette(prob_distribution, lambd)
            for k in range(0, lambd, 2):
                parents.append((pool[k:k+2], 2))
            return parents
        def make_reproduce(self, parents, _):
            # _ and len(parents) are both 2
            chromossomes = [p.chromossome for p in parents]
            if random.random() >= self.recombination_rate:
                return [self.Typ(c[:]) for c in chromossomes]
            children = self.recomb_xover(chromossomes)
            return [self.Typ(c[:]) for c in chromossomes]
        def survivor_selection(self, children):
            # len(children) is the same as len(self.members)
            self.select_genitor(children)

    return SGAIndividual, SGAPopulation



def GeneticAlgorithm(crlen, gene_mutation_rate=None, recombination_rate=None,
                     parentsel=None, use_elitism=True):

    if gene_mutation_rate is None:
        gene_mutation_rate = 1/crlen

    if recombination_rate is None:
        recombination_rate = 0.7

    BI, BP = EC.BinaryEC(crlen)

    if parentsel is None:
        parentsel = lambda self: self.parentsel_rank()

    class GAIndividual(BI):
        def __init__(self, chromossome=None):
            super().__init__(chromossome, gene_mutation_rate)

    class GAPopulation(BP):
        def __init__(self, n, Typ=GAIndividual):
            assert n % 2 == 0
            super().__init__(n, Typ, recombination_rate)
        def get_parents(self):
            parents = []
            lambd = self.size
            prob_distribution = parentsel(self)
            pool = self.getpool_sus(prob_distribution, lambd)
            for k in range(0, lambd, 2):
                parents.append((pool[k:k+2], 2))
            return parents
        def make_reproduce(self, parents, _):
            # _ and len(parents) are both 2
            chromossomes = [p.chromossome for p in parents]
            if random.random() >= self.recombination_rate:
                return [self.Typ(c[:]) for c in chromossomes]
            children = self.recomb_xover(chromossomes)
            return [self.Typ(c[:]) for c in chromossomes]
        def survivor_selection(self, children):
            # len(children) is the same as len(self.members)
            self.select_genitor(children, elitism=use_elitism)

    return GAIndividual, GAPopulation
