#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Evolutionary Computing

Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1
"""

import random
import math
import collections
import statistics

import numpy as np




###
###   BASE CLASSES
###

class Individual:
    def __init__(self, chromossome=None, gene_mutation_rate=None):
        self.age = 0
        self.chromossome = chromossome
        self.update_fitness()
        self.gene_mutation_rate = gene_mutation_rate
    def phenotype(self):
        """Should return a value that encodes the fitness of this specific
        individual to the evolution problem at hand."""
        return None
    def mutate(self):
        """Introduces a mutation on the individual."""
        pass
    def __lt__(self, other):
        return self.fitness < other.fitness
    def __le__(self, other):
        return (self < other) or (self.fitness == other.fitness)
    def __gt__(self, other):
        return not (self <= other)
    def __ge__(self, other):
        return not (self < other)
    # Do not implement __eq__ (so as to remain hashable)
    @staticmethod
    def gene_creep(g_now, get_creep, l, u, cycle):
        g = g_now + get_creep()
        if cycle:
            if g >= u:
                while g >= u: g -= u-l
            elif g < l:
                while g < l: g += u-l
        else:
            while g >= u or g < l:
                g = g_now + get_creep()
        return g
    def mutt_swap(self):
        """Swap mutation"""
        i, j = random.sample(range(len(self.chromossome)), 2)
        perm = self.chromossome
        perm[i], perm[j] = perm[j], perm[i]
    def mutt_insert(self):
        """Insert mutation"""
        i, j = random.sample(range(len(self.chromossome)), 2)
        if i > j: i,j = j,i
        g = self.chromossome.pop(j)
        self.chromossome.insert(i+1, g)
    def mutt_scramble(self):
        """Scramble mutation"""
        i, j = random.sample(range(len(self.chromossome)), 2)
        if i > j: i,j = j,i
        j += 1
    def mutt_inversion(self):
        """Inversion mutation"""
        i, j = random.sample(range(len(self.chromossome)), 2)
        if i > j: i,j = j,i
        j += 1
        self.chromossome[i:j] = reversed(self.chromossome[i:j])

class Population:

    def __init__(self, n, Typ=Individual, recombination_rate=0.5):
        """Initialises population of ‘n’ members of the species ‘Typ’. Typ
        should be a subclass of ‘Individual’."""
        self.Typ = Typ
        self.size = n;
        self.members = [Typ() for _ in range(self.size)]
        self.sort()
        self.best_genotype = self.members[-1]
        self.best_fitness = self.best_genotype.fitness
        self.generation = 0
        self.recombination_rate = recombination_rate

    def __repr__(self):
        return "<{} of size={}, best_genotype={}, generation {}a.>".format(
            type(self).__name__, self.size, self.best_genotype, self.generation
        )

    def sort(self):
        """Sorts the list of members according to fitness."""
        self.members.sort()

    ###
    ### Recombination methods
    ###

    @staticmethod
    def recomb_nxover(chromossomes, n):
        """n-point crossover for recombination"""
        cl = len(chromossomes[0])
        assert n < cl
        points = random.sample(range(1,cl), n)
        points.sort()
        points.insert(0,0)
        points.append(cl)
        ch = random.getrandbits(1)
        children = [[] for _ in chromossomes]
        for k in range(n+1):
            children[0] += chromossomes[ch^0][points[k]:points[k+1]]
            children[1] += chromossomes[ch^1][points[k]:points[k+1]]
            ch ^= 1
        return children

    @classmethod
    def recomb_xover(cls, chromossomes):
        """Canonical (1-point) crossover for recombination"""
        return cls.recomb_nxover(chromossomes, 1)

    @staticmethod
    def recomb_uniform_xover(chromossomes):
        """Uniform crossover"""
        children = [[] for _ in chromossomes]
        for k, (g, h) in enumerate(zip(*chromossomes)):
            ch = random.getrandbits(1)
            children[ch^0].append(g)
            children[ch^1].append(h)
        return children

    @staticmethod
    def get_symmetric_children(chromossomes, ratio=None, slc=None):
        if slc is None:
            slc = slice(len(chromossomes[0]))
        if ratio is None:
            ratio = lambda: random.uniform(0,1)
        ratio_list = (ratio() for _ in range(*slc.indices(slc.stop)))
        children = [c[:] for c in chromossomes]
        if random.getrandbits(1):
            children[0], children[1] = children[1], children[0]
        z = list(zip(ratio_list,
                     children[0][slc],
                     children[1][slc]))
        children[0][slc] = (  r  *g + (1-r)*h for r,g,h in z)
        children[1][slc] = ((1-r)*g +   r  *h for r,g,h in z)
        return children

    @classmethod
    def recomb_sar(cls, chromossomes, alpha=None):
        """Simple arithmetic recombination"""
        cl = len(chromossomes[0])
        crossover_point = random.randrange(1,cl)
        return cls.get_symmetric_children(
            chromossomes,
            ratio=alpha,
            slc=slice(crossover_point, cl)
        )

    @classmethod
    def recomb_1ar(cls, chromossomes, alpha=None):
        """Single arithmetic recombination"""
        k = random.randrange(len(chromossomes[0]))
        return cls.get_symmetric_children(
            chromossomes,
            ratio=alpha,
            slc=slice(k, k+1)
        )

    @classmethod
    def recomb_war(cls, chromossomes, alpha=None):
        """Whole arithmetic recombination"""
        return cls.get_symmetric_children(
            chromossomes,
            ratio=alpha,
            slc=slice(len(chromossomes[0]))
        )

    @classmethod
    def recomb_blx(cls, chromossomes, alpha=None):
        """Blend crossover (BLX-alpha)"""
        if alpha is None:
            alpha = random.expovariate(2)
        gamma = lambda: random.uniform(-alpha, 1 + alpha)
        return cls.get_symmetric_children(
            chromossomes,
            ratio=gamma,
            slc=slice(len(chromossomes[0]))
        )

    @staticmethod
    def single_pmx(chr0, chr1, i, j):
        child = [None for _ in chr0]
        child[i:j] = chr0[i:j]
        for k in range(i,j):
            g = chr1[k]
            if g in child:
                continue
            l = chr1.index(chr0[k])
            while not (child[l] is None):
                l = chr1.index(chr0[l])
            child[l] = g
        for k in range(len(child)):
            if child[k] is None:
                child[k] = chr1[k]
        return child

    @classmethod
    def recomb_pmx(cls, chromossomes):
        """Partially mapped crossover"""
        i, j = random.sample(range(len(self.chromossome)), 2)
        if i > j: i,j = j,i
        j += 1
        children = [None for _ in chromossomes]
        ch = random.getrandbits(1)
        children[ch^0] = cls.single_pmx(chromossomes[0], chromossomes[1], i, j)
        children[ch^1] = cls.single_pmx(chromossomes[1], chromossomes[0], i, j)
        return children

    @staticmethod
    def recomb_edge(chromossomes, cyclic=True):
        """Edge crossover"""
        cl = len(chromossomes[0])
        if cl <= 1:
            return [chromossomes[0][:]]
        if cl <= 2:
            return random.sample(chromossomes[0][:], 2)
        child = []
        # Step 1: construct edge table
        edges = {}
        def update_counter(ctr, chromossome, idx):
            if idx == 0:
                if cyclic:
                    ctr[chromossome[-1]] += 1
                ctr[chromossome[1]] += 1
            elif idx == cl-1:
                if cyclic:
                    ctr[chromossome[0]] += 1
                ctr[chromossome[idx-1]] += 1
            else:
                ctr[chromossome[idx-1]] += 1
                ctr[chromossome[idx+1]] += 1
        for k in range(cl):
            allele = chromossomes[0][k]
            l = chromossomes[1].index(allele)
            edges[allele] = collections.Counter()
            update_counter(edges[allele], chromossomes[0], k)
            update_counter(edges[allele], chromossomes[1], l)
        # Step 2:
        child.append(random.choice(chromossomes[0]))
        while len(child) < cl:
            # Step 3: current = child[-1]
            # Step 4: Remove references to current
            for a in edges:
                del edges[a][child[-1]]
            # Step 6:
            if len(edges[child[-1]]) == 0:
                # TODO: implement full step 6
                g = random.choice(list(edges))
                while g == child[-1]:
                    g = random.choice(list(edges))
                child.append(g)
                continue
            # Step 5: Examine edges[child[-1]]
            mc = edges[child[-1]].most_common() # sort by num of occurr.
            n = mc[0][1] # best value
            candidates = [(tup[0], len(edges[tup[0]]))
                          for tup in mc
                          if tup[1] == n] # remove losers
            candidates.sort(key=lambda tup: tup[1]) # sort by length
            n = candidates[0][1] # best value
            final_cand = [tup[0]
                          for tup in candidates
                          if tup[1] == n] # remove losers
            child.append(random.choice(final_cand))
            del edges[child[-2]]
        return [child]

    # TODO: implement ORDER and CYCLE crossover for permutation
    #       representation

    @staticmethod
    def recomb_cut_crossfill(chromossomes):
        i = random.randrange(1,len(chromossomes[0])) # crossover point
        children = [c[:i] for c in chromossomes]
        for g in chromossomes[0]:
            if g not in children[1]:
                children[1].append(g)
        for g in chromossomes[1]:
            if g not in children[0]:
                children[0].append(g)
        return children

    ###
    ### Parent selection methods
    ###

    def parentsel_fps(self, *, beta_t=None, sigma_scale=None):
        """Fitness proportional selection

        If sigma_scale is None, we just subtract the value of the
        least-fit member. If beta_t is provided, it is used in place of
        the worst fitness.

        If sigma_scale is provided, sigma scaling is performed, and
        beta_t is ignored (if it is given at all).

        Implementation detail: self.members must be sorted!!"""
        if sigma_scale is None:
            if beta_t is None:
                # Find worst fitness. Gotcha: if all fitnesses are the
                # same, beta_t := worst_fitness-1.
                beta_t = self.members[0].fitness
                if beta_t == self.members[-1].fitness:
                    beta_t -= 1
            trans_fits = [max(ind.fitness - beta_t, 0) for ind in self.members]
        else:
            fits = [ind.fitness for ind in self.members]
            mean = statistics.mean(fits)
            std = statistics.pstdev(fits, mean)
            beta_t = mean - sigma_scale*std
            trans_fits = [max(f - beta_t, 0) for f in fits]
        s = sum(trans_fits)
        return [f/s for f in trans_fits]

    def parentsel_rank(self, *, s=1.5, transf=None):
        """Rank-based parent selection

        `transf` is a transformation function. If it is provided, it is
        used, and then values are normalized, and `s` will be ignored.

        If `transf` is not provided, a linear mapping is used, with
        factor `s`.

        Examples:
            pop.parentsel_rank() # linear, s=1.5
            pop.parentsel_rank(s=2) # linear, s=2.0
            pop.parentsel_rank(transf=lambda i: 1-math.exp(-i)) # exp
            pop.parentsel_rank(transf=lambda i: math.exp(i)) # inverse exp
        """
        mu = self.size
        if transf is None:
            transf = lambda i: 2 - s + 2*i*(s-1)/(mu-1)
        trans_fits = [transf(i) for i in range(mu)]
        s = sum(trans_fits)
        return [f/s for f in trans_fits]

    def parentsel_uniform(self):
        p = 1/self.size
        return [p for _ in self.members]

    def getpool_roulette(self, distribution, lambd, cumulative=False):
        """Roulette wheel algorithm for sampling a mating pool"""
        if not cumulative:
            cumdist = []
            cum = 0
            for p in distribution:
                cum += p
                cumdist.append(cum)
            distribution = cumdist
        pool = []
        for k in range(lambd):
            r = random.random()
            for i in range(len(distribution)):
                if distribution[i] >= r:
                    break
            pool.append(self.members[i])
        return pool

    def getpool_sus(self, distribution, lambd, cumulative=False):
        """Stochastic universal sampling algorithm for a mating pool"""
        if not cumulative:
            cumdist = []
            cum = 0
            for p in distribution:
                cum += p
                cumdist.append(cum)
            distribution = cumdist
        pool = []
        width = 1.0/lambd
        r = random.uniform(0, width)
        k = 0
        i = 0
        while k < lambd:
            while r <= distribution[i]:
                pool.append(self.members[i])
                r += width
                k += 1
            i += 1
        return pool

    def getpool_tournament(self, lambd, k=2):
        pool = []
        for l in range(lambd):
            tour = (random.choice(self.members) for _ in range(k))
            # `tour` is a sample with replacement of k members
            best = next(tour)
            for opponent in tour:
                if best < opponent:
                    best = opponent
            pool.append(best)
        return pool

    ###
    ### Survivor selection methods
    ###

    # TODO: In the following methods which accept an 'elitism' keyword
    #       argument, change it so that this argument gets an integer
    #       instead of a boolean, and so that we can get an elitism
    #       that preserver the "n" best parents.

    def select_genitor(self, children, discard_children=False, elitism=False):
        """GENITOR method

        'lambda' is len(children) and 'mu' is len(self.members)

        To get a (mu + lambda) selection strategy, use
        discard_children=True.

        To get a steady-state model, use lambda << mu

        To get a generational model, use lambda = mu and
        discard_children=False .

        If lambda > mu, discard_children will be ignored (and treated as
        True)
        """
        lambd = len(children)
        mu = self.size
        if lambd > mu:
            discard_children = True
        if elitism and not discard_children and lambd == mu:
            best_child = children[0]
            for other_child in children[1:]:
                if other_child > best_child:
                    best_child = other_child
            if self.members[-1] > best_child:
                del children[random.randrange(lambd)]
                lambd -= 1
        if not discard_children:
            del self.members[:lambd]
        self.members[0:0] = children
        self.sort()
        if discard_children:
            del self.members[:lambd]

    def select_mulambd(self, children, elitism=False):
        """(mu, lambda) strategy

        lambda must be >= mu"""
        lambd = len(children)
        mu = self.size
        if elitism and mu > 0:
            best_parent = self.members[0]
            for other_parent in self.members[1:]:
                if other_parent > best_parent:
                    best_parent = other_parent
        self.members = children
        if elitism and mu > 0:
            k = random.randrange(mu)
            self.members[k] = best_parent
        self.members.sort()
        del self.members[:lambd-mu]

    def select_rrtour(self, children, q=10, elitism=False, sort=True):
        """Round-robin tournament"""
        lambd=len(children)
        if elitism and self.size > 0:
            best_parent = self.members[0]
            for other_parent in self.members[1:]:
                if other_parent > best_parent:
                    best_parent = other_parent
        pool = self.members + children
        results = []
        for candidate in pool:
            wins = 0
            for _ in range(q):
                if candidate > random.choice(pool):
                    wins += 1
            results.append((candidate, wins))
        results.sort(key=lambda tup: tup[1])
        del results[:lambd]
        if (elitism and self.size > 0
                    and results[0][1] == results[-1][1]
                    and best_parent not in (tup[0] for tup in results)):
            k = rand.randrange(len(results))
            results[k] = (best_parent, 0)
        self.members = [tup[0] for tup in results]
        if sort:
            self.sort()

    # TODO: implement survivor selection algorithms from section 5.3

    ###
    ### Multimodal problems
    ###

    # TODO: implement algorithms for multimodal probs from section 5.5

    ###
    ### Evolution components
    ###

    def get_parents(self):
        """Should return a list of pairs (parents, M). In each pair,
        ‘parents’ is a list of the members which will form a group of
        parents; and ‘M’ is just a natural number indicating how many
        children that group of parents will have. For assexual
        reproduction, ‘parents’ will be a singleton; for ordinary sexual
        reproduction, ‘parents’ will have two elements; and for
        multiparent reproduction, the list ‘parents’ will have more than
        two elements."""
        return []

    def make_reproduce(parents, m):
        """Given a set of parents, returns a list of ‘m’ children, i.e.,
        new individuals generated using the genes of the parents."""
        return [None]*m

    def survivor_selection(self, children):
        """Given a list of children of the next generation, substitute
        some (or all) of the current members of the population by some,
        or all, of the new children."""
        pass

    def evolve(self, enough, print_at=None, mkprint=None):
        if mkprint is None:
            mkprint = lambda self: print(self.generation, end=' ', flush=True)
        while not enough(self):
            if print_at is not None and self.generation % print_at == 0:
                mkprint(self)
            for m in self.members:
                m.age += 1
            selected_groups = self.get_parents()
            children = []
            for parents, m in selected_groups:
                children += self.make_reproduce(parents, m)
            for c in children:
                c.mutate()
            self.survivor_selection(children)
            self.sort()
            self.generation += 1
            new_best = self.members[-1].fitness
            if new_best >= self.best_fitness:
                self.best_genotype = self.members[-1]
                self.best_fitness = new_best




###
###   BINARY REPRESENTATION
###

def bin_from_gray(g):
    b = [g[-1]]
    for n in g[-2::-1]:
        b.insert(0, b[0]^n)
    return b

def BinaryEC(crlen):

    # Binary Individual
    class BI(Individual):
        chromossome_size = crlen
        def __init__(self, chromossome=None, gene_mutation_rate=None):
            if chromossome is None:
                chromossome = [
                    bool(b) for b in
                    np.random.randint(2, size=self.chromossome_size)
                ]
            if gene_mutation_rate is None:
                gene_mutation_rate = 1/self.chromossome_size
            super().__init__(chromossome, gene_mutation_rate)
        def __repr__(self):
            fmt = "<Binary Individual with genotype={}, fitness {}, age {}>"
            g = ''.join(str(int(b)) for b in reversed(self.chromossome))
            return fmt.format(g, self.fitness, self.age)
        def mutate(self):
            for k in range(len(self.chromossome)):
                if random.random() < self.gene_mutation_rate:
                    self.chromossome[k] ^= True # flip bit
            self.update_fitness()

    # Binary Population
    class BP(Population):
        def __init__(self, n, Typ=BI, recombination_rate=None):
            if recombination_rate is None:
                super().__init__(n, Typ)
            else:
                super().__init__(n, Typ, recombination_rate)

    return BI, BP




###
###   INTEGER REPRESENTATION
###

def IntegerEC(crlen, val, *args):
    """Usage: IntegerEC(chromossome_length, L, U).
    IntegerEC(crlen, N) is the same as IntegerEC(crlen, 0, N)."""

    if len(args) == 0:
        L = 0
        U = val
    elif len(args) == 1:
        L = val
        U = args[0]
    else:
        raise ValueError("IntegerEC is used as IntegerEC(crlen, L, U)")
    assert U > L

    # Integer Individual
    class II(Individual):
        chromossome_size = crlen
        def __init__(self, chromossome=None, gene_mutation_rate=None):
            if chromossome is None:
                chromossome = [
                    int(ni) for ni in
                    L + np.random.randint(U-L, size=self.chromossome_size)
                ]
            if gene_mutation_rate is None:
                gene_mutation_rate = 1/self.chromossome_size
            super().__init__(chromossome, gene_mutation_rate)
        def __repr__(self):
            return ("<Integer ({}-{}) Individual with "
                    "genotype={}, fitness {}, age {}>").format(
                L, U, self.chromossome, self.fitness, self.age
            )
        def mutt_rr(self):
            """Random reset"""
            for k in range(len(self.chromossome)):
                if random.random() < self.gene_mutation_rate:
                    self.chromossome[k] = L + random.randrange(U-L)
        @staticmethod
        def creep(get_creep, cycle=True):
            """Usage:
                class MyII(II):
                    creep_me = II.creep(get_creep)
                    def mutate(self):
                        self.creep_me()
                        self.update_fitness()
            """
            def creep_me(self):
                for k in range(len(self.chromossome)):
                    if random.random() < self.gene_mutation_rate:
                        g = self.chromossome[k] + get_creep()
                        if cycle:
                            if g >= U:
                                while g >= U: g -= U-L
                            elif g < L:
                                while g < L: g += U-L
                        else:
                            if g >= U:
                                g = U
                            elif g < L:
                                g = L
                        self.chromossome[k] = g
            return creep_me

    # Integer Population
    class IP(Population):
        def __init__(self, n, Typ=II, recombination_rate=None):
            if recombination_rate is None:
                super().__init__(n, Typ)
            else:
                super().__init__(n, Typ, recombination_rate)

    return II, IP




###
###   FLOATING POINT REPRESENTATION
###

def FloatEC(L, U,
            adap_tau_i=None, adap_beta=(5/360)*(2*math.pi),
            adap_tau=None, sigma_eps0=1e-10):
    """Evolutionary computing with floating-point representation.

    Return a class for float-individuals and another for populations of
    float-individuals.

    L and U are lists of lower and upper bounds for chromossome
    coordinates. Every valid chromossome c must satisfy L <= c <= U.

    * Uniform mutation

        FI, FP = FloatEC(L,U)
        class MyFI(FI):
            def mutate(self):
                self.mutt_rr()
                self.update_fitness()
        x = MyFI(gene_mutation_rate=my_gmr) # gmr should be specified

    * Nonuniform nonapdaptive mutation

        FI, FP = FloatEC(L,U)
        class MyFI(FI):
            creep_me = FI.creep(my_get_creep)
            def mutate(self):
                self.creep_me()
                self.update_fitness()
        x = MyFI() # gene mutation rate may be specified, if desired

    * Self-adaptive uncorrelated mutation with one step size (spheric)

        FI, FP = FloatEC(L,U, adap_tau=T/sqrt(n), sigma_eps0=1e-15)
        # adap_tau and sigma_eps0 may be omitted
        class MyFI(FI):
            def mutate(self):
                self.mutt_usph()
                self.update_fitness()
        x = MyFI() # gene mutation rate is ignored

    * Self-adaptive uncorrelated mutation with n step sizes (elliptic)

        FI, FP = FloatEC(L,U, sigma_eps0=1e-15
                         adap_tau=T/n^0.5,
                         adap_tau_i=T2/n^0.25)
        class MyFI(FI):
            def mutate(self):
                self.mutt_uelp()
                self.update_fitness()
        x = MyFI()
    """

    assert len(L) == len(U)
    min_width = min(u-l for u,l in zip(U,L))

    if adap_tau is None:
        adap_tau = 1/math.sqrt(len(L))
    if adap_tau_i is None:
        adap_tau_i = math.sqrt(adap_tau)

    # Float Individual
    class FI(Individual):
        chromossome_size = len(L)
        evol_parameters = {'usph_sigma': 'scalar', 'uelp_sigma': 'vec'}
        def __init__(self, chromossome=None,
                     usph_sigma_init=None, uelp_sigma_init=None,
                     cov_matrix_init=None,
                     gene_mutation_rate=1.0):
            if chromossome is None:
                chromossome = [random.uniform(l,u) for l,u in zip(L,U)]
            if usph_sigma_init is None:
                usph_sigma_init = (min_width/10) \
                                  * random.lognormvariate(0, adap_tau);
            if uelp_sigma_init is None:
                uelp_sigma_init = [
                    usph_sigma_init * random.lognormvariate(0, adap_tau_i)
                    for _ in chromossome
                ]
            #if cov_matrix_init is None:
                ## TODO: non-positive???
                #cov_matrix_init = [None for _ in uelp_sigma_init]
                #alphas          = [[]   for _ in uelp_sigma_init]
                #for k in range(len(cov_matrix_init)):
                    #cov_matrix_init[k] = [0 for _ in uelp_sigma_init]
                    #for l in range(k):
                        #alphas[k].append(random.uniform(-math.pi, math.pi))
                        #cov_matrix_init[k][l] = (
                            #0.5
                            #* (uelp_sigma_init[k]**2 - uelp_sigma_init[l]**2)
                            #* math.tan(2*alphas[k][l])
                        #)
                        #cov_matrix_init[l][k] = cov_matrix_init[k][l]
                    #cov_matrix_init[k][k] = uelp_sigma_init[k]**2
            self.usph_sigma = usph_sigma_init
            self.uelp_sigma = uelp_sigma_init
            self.cov_matrix = cov_matrix_init
            super().__init__(chromossome, gene_mutation_rate)
        def __repr__(self):
            fmt = "<Float Individual with genotype=[{}], fitness {}, age {}>"
            g = ', '.join("{:.3}".format(f) for f in self.chromossome)
            return fmt.format(g, self.fitness, self.age)
        def mutt_rr(self):
            """Uniform mutation"""
            for k in range(len(self.chromossome)):
                if random.random() < self.gene_mutation_rate:
                    self.chromossome[k] = random.uniform(L[k], U[k])
        @staticmethod
        def creep(get_creep=None, cycle=False):
            """Nonuniform mutation"""
            if get_creep is None:
                get_creep = lambda: random.normalvariate(0, min_width/10)
            def creep_me(self):
                for k in range(len(self.chromossome)):
                    if random.random() < self.gene_mutation_rate:
                        self.chromossome[k] = self.gene_creep(
                            self.chromossome[k], get_creep, L[k], U[k], cycle
                        )
            return creep_me
        def mutt_usph(self):
            """Uncorrelated mutation with one step size (spheric)"""
            self.usph_sigma *= random.lognormvariate(0, adap_tau)
            if self.usph_sigma < sigma_eps0:
                self.usph_sigma = sigma_eps0
            for k in range(len(self.chromossome)):
                # Gene mutation rate is always 1.0 for self-adaptive
                # mutation algorithms.
                self.chromossome[k] = self.gene_creep(
                    self.chromossome[k],
                    lambda: random.normalvariate(0, self.usph_sigma),
                    L[k], U[k],
                    True
                )
        def mutt_uelp(self):
            """Uncorrelated mutation with n step sizes (elliptic)"""
            global_factor = random.lognormvariate(0, adap_tau/math.sqrt(2))
            for k in range(len(self.uelp_sigma)):
                self.uelp_sigma[k] *= global_factor
                self.uelp_sigma[k] *= random.lognormvariate(
                    0, adap_tau_i/math.sqrt(2)
                )
                if self.uelp_sigma[k] < sigma_eps0:
                    self.uelp_sigma[k] = sigma_eps0
            for k in range(len(self.chromossome)):
                # Gene mutation rate is always 1.0 for self-adaptive
                # mutation algorithms.
                self.chromossome[k] = self.gene_creep(
                    self.chromossome[k],
                    lambda: random.normalvariate(0, self.uelp_sigma[k]),
                    L[k], U[k],
                    True
                )
        def mutt_celp(self):
            """Correlated mutation (elliptic)"""
            # LN(0, tau/sqrt(2))^2 == LN(0, tau*sqrt(2))
            global_factor = random.lognormvariate(0, adap_tau*math.sqrt(2))
            for k in range(len(self.cov_matrix)):
                self.cov_matrix[k][k] *= global_factor
                self.cov_matrix[k][k] *= random.lognormvariate(
                    0, adap_tau_i*math.sqrt(2)
                )
                if math.sqrt(self.cov_matrix[k][k]) < sigma_eps0:
                    self.cov_matrix[k][k] = sigma_eps0**2
                for l in range(k):
                    alphas[k][l] += random.normalvariate(0, adap_beta)
                    while abs(alphas[k][l]) > math.pi:
                        alphas[k][l] -= copysign(2*math.pi, alphas[k][l])
                    cov_matrix_init[k][l] = (
                        0.5
                        * (self.cov_matrix[k][k]**2 - self.cov_matrix[l][l]**2)
                        * math.tan(2*alphas[k][l])
                    )
            # TODO

    # Float Population
    class FP(Population):
        def __init__(self, n, Typ=FI, recombination_rate=None):
            if recombination_rate is None:
                super().__init__(n, Typ)
            else:
                super().__init__(n, Typ, recombination_rate)
        @staticmethod
        def get_symmetric_children(chromossomes, ratio=None, slc=None):
            children = super(FP, FP).get_symmetric_children(
                chromossomes, ratio, slc)
            if ratio is None:
                ratio = lambda: random.uniform(0,1)
            for k in range(len(chromossomes[0])):
                while not (
                    L[k] <= children[0][k] < U[k] and
                    L[k] <= children[1][k] < U[k]
                ):
                    r = ratio()
                    children[0][k] = (  r  *chromossomes[0][k] +
                                      (1-r)*chromossomes[1][k])
                    children[1][k] = ((1-r)*chromossomes[0][k] +
                                        r  *chromossomes[1][k])
            return children


    return FI, FP




###
###   PERMUTATION REPRESENTATION
###

def PermutationEC(crlen):
    """Evolutionary computing with permutation representation.

    Return a class for permutation-individuals and another for
    populations of permutation-individuals.
    """

    # Permutation Individual
    class PI(Individual):
        chromossome_size = crlen
        def __init__(self, chromossome=None, mutation_rate=None):
            if chromossome is None:
                chromossome = random.sample(range(self.chromossome_size),
                                            self.chromossome_size)
            if mutation_rate is None:
                mutation_rate = 0.5
            self.mutation_rate = mutation_rate
            super().__init__(chromossome)
        def __repr__(self):
            fmt = ("<Permutation Individual with genotype={}, fitness {}, "
                   "age {}>")
            g = '-'.join(str(k) for k in self.chromossome)
            return fmt.format(g, self.fitness, self.age)

    # Permutation Population
    class PP(Population):
        def __init__(self, n, Typ=PI, recombination_rate=None):
            if recombination_rate is None:
                super().__init__(n, Typ)
            else:
                super().__init__(n, Typ, recombination_rate)


    return PI, PP


# TODO: TREE REPRESENTATION (section 4.6, pag 7.5)
