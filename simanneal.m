function [x_plot, js_plot, x_min_plot, x_min, J_min] = ...
    simanneal(x0, N, J, R, T, T_plot)
w = waitbar(0);

% TODO: adaptive step size
% TODO: adaptive cooling schedule (procede to next temperature only when
%       current distribution has converged)

x_plot  = cell(1, length(T_plot));
js_plot = zeros(N, length(T_plot));
x_min_plot = zeros(size(x0,1), length(T_plot));

x_min = x0;
J_min = J(x0);

k_plot = 1;
for k=1:length(T);

    waitbar((k-1)/length(T), w, sprintf('Temperatura #%d: %f', k-1, T(k)));
    [X, js, this_x_min, this_J_min] = metropolis(x0, N, J, R, T(k));
    waitbar(k/length(T), w, sprintf('Temperatura #%d: %f', k-1, T(k)));

    if this_J_min < J_min
        x_min = this_x_min;
        J_min = this_J_min;
    end

    x0 = X(:,end);

    if find(T_plot == k)
        x_plot{k_plot} = X;
        js_plot(:,k_plot) = js';
        x_min_plot(:,k_plot) = x_min;
        k_plot = k_plot + 1;
    end

end

delete(w);
