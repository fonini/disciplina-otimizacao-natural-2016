function J = J_eg1(x, lambda)
x = reshape(x,2,[]);
P = size(x,2);
JA = mean(sum(x.^2));
JB = 0;
for i=1:P,
    for j=(i+1):P,
        JB = JB + 1/sum((x(:,i)-x(:,j)).^2);
    end;
end;
JB = JB/(P*(P-1)/2);
J = JA + lambda*JB;
