#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Lista 06, Exercício 2
"""

import math
import statistics
import sys
import shelve, datetime

import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import EC
import GA

import shelve_helper

# TODO: make the script put everything in a new directory, instead of
#       polluting the main directory with shelve__* and *.eps files.
#       The new directory should be named {timestamp}__{title}, or
#       similarly.

L = 25

IndivíduoGA, PopulaçãoGA = GA.GeneticAlgorithm(
    L,
    parentsel=lambda self: self.parentsel_fps(),
    use_elitism=False
)

class IndivíduoOneMax(IndivíduoGA):
    def update_fitness(self):
        self.fitness = sum(self.chromossome)
    def mutate(self):
        super().mutate()
        # Local search:
        curr_chr = self.chromossome
        curr_fit = self.fitness
        best_chr = curr_chr
        best_fit = self.fitness
        for g in range(L):
            self.chromossome = curr_chr[:]
            self.chromossome[g] ^= 1
            self.update_fitness()
            if self.fitness > best_fit:
                best_chr = self.chromossome
                best_fit = self.fitness
        self.chromossome = best_chr
        self.fitness = best_fit

class PopulaçãoOneMax(PopulaçãoGA):
    def __init__(self, *args, **kwargs):
        if 'Typ' not in kwargs:
            kwargs['Typ'] = IndivíduoOneMax
        super().__init__(*args, **kwargs)

def main(max_gerações, shelve_title=None, save=True):

    categoria = 'ex2'

    pop = PopulaçãoOneMax(100)

    médias = []
    percentis = [0, 10, 25, 75, 90, 100]
    fit_pils = {p: [] for p in percentis}
    def get_percentil(p, seq):
        N = len(seq)
        idx_float = (p/100)*(N-1)
        idx, frac = divmod(idx_float, 1)
        idx = int(idx)
        if idx+1 == N:
            return seq[-1]
        return frac*seq[idx+1] + (1-frac)*seq[idx]

    for k in range(max_gerações):
        pop.evolve(lambda p: p.generation >= k)
        # After generation k-th, we have:
        fit_list = [m.fitness for m in pop.members]
        médias.append(statistics.mean(fit_list))
        for p in percentis:
            fit_pils[p].append(get_percentil(p, fit_list))
        if max(fit_list) == L:
            break

    k += 1
    timestamp = datetime.datetime.utcnow().strftime(
        shelve_helper.globts.string)

    print(pop)
    print(k)

    plt.figure()
    plt.plot(range(k), médias, 'b')
    plt.plot(range(k), fit_pils[0], 'g') # min
    plt.plot(range(k), fit_pils[100], 'g') # max
    p1 = plt.fill_between(range(k),
                          fit_pils[percentis[1]],
                          fit_pils[percentis[-2]],
                          facecolor='0.85')
    p2 = plt.fill_between(range(k),
                          fit_pils[percentis[2]],
                          fit_pils[percentis[-3]],
                          facecolor='0.65')
    p1.set_linewidth(0)
    p2.set_linewidth(0)
    plt.xlabel('Geração')
    plt.ylabel('Fitness')
    plt.title('Evolução do problema OneMax com $L={}$'.format(L))
    plt.grid(True)
    plt.savefig(categoria+'_evol__'+timestamp+'.eps')

    if not save:
        return

    # Variáveis para serem salvas
    variáveis = {
        'max_gerações': ...,
        'k':            ...,
        'médias':       ...,
        'fit_pils':     ...,
        'pop':          ...,
    }

    if shelve_title is None:
        shelve_title = ''
    else:
        shelve_title = '__' + shelve_title
    filename = 'shelve__' + timestamp + shelve_title

    print("Salvando dados no arquivo:  {}".format(filename))
    with shelve.open(filename) as shv:
        for k in variáveis:
            if variáveis[k] is Ellipsis:
                shv[k] = vars()[k]
            else:
                shv[k] = variáveis[k]
    print("Dados salvos. Fim.")

if __name__ == '__main__':

    import argparse
    import textwrap
    import os
    import os.path
    import glob

    parser = argparse.ArgumentParser(description="Exercício 2 da lista 06.")
    parser.add_argument(
        "title",
        help="Título da shelve que será usada para guardar os"
             " resultados do programa.",
        nargs='?',
        metavar="TITLE",
    )
    parser.add_argument(
        "-o", "--open",
        help="Abrir uma shelve.",
        nargs='?',
        metavar="FILE",
        const=Ellipsis,
    )
    parser.add_argument(
        "-g", "--generations",
        help="Quantidade máxima de gerações do GA.",
        type=int,
        default=25,
    )
    args = parser.parse_args()

    tw = textwrap.TextWrapper(subsequent_indent='      ')

    if  args.open is not None  and  args.title is not None:
        msg = (
            "Escolha apenas uma das opções: ou executa o algoritmo (e"
            " salva os resultados na shelve) ou abre a shelve usando a"
            " flag '-o'."
        )
        print(tw.fill("ERRO: " + msg), file=sys.stderr)
        sys.exit(1)

    if args.open is None:
        main(args.generations, shelve_title=args.title)
        sys.exit(0)

    # Com certeza args.title é None, e args.open não é None
    filename = args.open
    if filename is Ellipsis:
        print(tw.fill(
            "Buscando última shelve usada. O nome dela estará"
            " disponível na variável ‘filename’."
        ))
        filename = shelve_helper.get_last_shelve()
    if not os.path.isfile(filename):
        # Tratar args.open como título
        candidates = shelve_helper.get_shelves(args.open)
        if len(candidates) == 0:
            msg = "Não existe uma shelve com título ‘{}’.".format(
                args.open)
            print(tw.fill("ERRO: " + msg), file=sys.stderr)
            sys.exit(2)
        elif len(candidates) > 1:
            msg = "Existe mais de uma shelve com título ‘{}’:".format(
                args.open)
            print(tw.fill("ERRO: " + msg), file=sys.stderr)
            for c in candidates:
                print('  > ' + c, file=sys.stderr)
            sys.exit(3)
        # here, len == 1
        filename = candidates[0]
        del candidates
    with shelve.open(filename) as shv:
        for k in shv:
            globals()[k] = shv[k]
    del parser, k, tw, args
    os.environ['PYTHONINSPECT'] = '1'
