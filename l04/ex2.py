#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Exercício 2
"""

import statistics

import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import EC
import GA

L = 25

IndivíduoGA, PopulaçãoGA = GA.GeneticAlgorithm(
    L,
    parentsel=lambda self: self.parentsel_fps(),
    use_elitism=False
)

class IndivíduoOneMax(IndivíduoGA):
    def update_fitness(self):
        self.fitness = sum(self.chromossome)


pop = PopulaçãoGA(100, IndivíduoOneMax)

best = []
worst = []
mean = []

for k in range(10000):
    pop.evolve(lambda p: p.generation >= k)
    # After generation k-th, we have:
    best.append(pop.members[-1].fitness)
    worst.append(pop.members[0].fitness)
    mean.append(statistics.mean(m.fitness for m in pop.members))
    if best[-1] == L:
        break

k += 1


plt.plot(range(k),worst)
plt.plot(range(k),mean,'-o')
plt.plot(range(k),best)
plt.xlabel('Geração')
plt.ylabel('Fitness')
plt.title('Evolução do problema OneMax com $L={}$'.format(L))
plt.grid(True)
plt.show()


print(pop)
print(best)
print(k)
