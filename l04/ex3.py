#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Exercício 3
"""

import math
import statistics
import sys
import shelve, datetime

import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import EC
import ES

import shelve_helper

n = 30

IndivíduoES, PopulaçãoES = ES.EvolutionStrategy(
    [-30 for _ in range(n)],
    [+30 for _ in range(n)],
    mu=30, lambd=200
)

class IndivíduoAckley(IndivíduoES):
    def update_fitness(self):
        norm2 = sum(x*x for x in self.chromossome)
        sum_cos = sum(math.cos(2*math.pi*x) for x in self.chromossome)
        self.fitness = 0
        self.fitness -= 20 * math.exp(-0.2*math.sqrt(norm2/n))
        self.fitness -= math.exp(sum_cos/n)
        self.fitness += 20 + math.exp(1)
        self.fitness *= -1

class PopulaçãoAckley(PopulaçãoES):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, Typ=IndivíduoAckley, **kwargs)
    def survivor_selection(self, children):
        super().survivor_selection(children)
        self.sort()
        self.sigma_ev.append(statistics.mean(self.members[-1].uelp_sigma))

def main(realizações=100, gerações=800, shelve_title=None, save=True):

    best = []
    sigma_evs = []
    for k in range(realizações):
        print("Calculando {:3}a. rodada...".format(k), end='')
        pop = PopulaçãoAckley()
        pop.evolve(lambda p: p.generation >= gerações)
        best.append(pop.best_genotype)
        sigma_evs.append(pop.sigma_ev)
        print("\n{}".format(pop))
        for m in pop.members:
            print(m)
            print(m.uelp_sigma)
        print("====================================================\n"*10)

    bfits = [-b.fitness for b in best]
    bsigmas = [statistics.mean(b.uelp_sigma) for b in best]
    timestamp = datetime.datetime.utcnow().strftime(
        shelve_helper.globts.string)

    print("###############################################################")

    plt.figure()
    plt.hist(bfits, 16)
    plt.xlabel('Melhores valores encontrados')
    plt.grid(True)
    plt.savefig('ex3_best_val__'+timestamp+'.eps')

    print(bfits)
    print(statistics.mean(bfits))
    print(statistics.stdev(bfits))

    print("###############################################################")

    plt.figure()
    plt.hist(bsigmas, 16)
    plt.xlabel('Passo médio')
    plt.grid(True)
    plt.savefig('ex3_best_sigma__'+timestamp+'.eps')

    print(bsigmas)
    print(statistics.mean(bsigmas))
    print(statistics.stdev(bsigmas))

    print("###############################################################")

    plt.figure()
    for k in range(realizações):
        plt.semilogy(range(gerações), sigma_evs[k], 'b')
    plt.xlabel('Geração')
    plt.ylabel('Passo médio do indivíduo mais adaptado')
    plt.grid(True)
    plt.savefig('ex3_evol_sigma__'+timestamp+'.eps')

    print(sigma_evs)

    print("###############################################################")

    plt.figure()
    plt.plot([statistics.mean(m.uelp_sigma) for m in pop.members],
            [-m.fitness for m in pop.members],
            linestyle='None', marker='o', markersize=5, color='b')
    plt.xlabel('Passo médio do indivíduo')
    plt.ylabel('Fitness')
    plt.grid(True)
    plt.savefig('ex3_last_pop__'+timestamp+'.eps')

    print([statistics.mean(m.uelp_sigma) for m in pop.members])
    print([-m.fitness for m in pop.members])

    print("###############################################################")

    plt.figure()
    plt.plot([math.sqrt(sum(g*g for g in m.chromossome)) for m in pop.members],
             [-m.fitness for m in pop.members],
             linestyle='None', marker='o', markersize=5, color='b')
    plt.xlabel("``Norma'' do cromossomo")
    plt.ylabel('Fitness')
    plt.grid(True)
    plt.savefig('ex3_last_pop_corr__'+timestamp+'.eps')

    print([math.sqrt(sum(g*g for g in m.chromossome)) for m in pop.members])
    print([-m.fitness for m in pop.members])

    if not save:
        return
    print("###############################################################")

    # Variáveis para serem salvas
    variáveis = {
        'realizações': ...,
        'gerações':    ...,
        'best':        ...,
        'sigma_evs':   ...,
        'last_pop':    pop,
    }

    if shelve_title is None:
        shelve_title = ''
    else:
        shelve_title = '__' + shelve_title
    filename = 'shelve__' + timestamp + shelve_title
    print("Salvando dados no arquivo:  {}".format(filename))
    with shelve.open(filename) as shv:
        for k in variáveis:
            if k is Ellipsis:
                shv[k] = vars()[k]
            else:
                shv[k] = variáveis[k]
    print("Dados salvos. Fim.")

if __name__ == '__main__':

    import argparse
    import textwrap
    import os
    import os.path
    import glob

    parser = argparse.ArgumentParser(description="Exercício 3 da lista 04.")
    parser.add_argument(
        "title",
        help="Título da shelve que será usada para guardar os"
             " resultados do programa.",
        nargs='?',
        metavar="TITLE",
    )
    parser.add_argument(
        "-o", "--open",
        help="Abrir uma shelve.",
        nargs='?',
        metavar="FILE",
        const=Ellipsis,
    )
    parser.add_argument(
        "-r", "--runs",
        help="Quantidade de realizações do algoritmo.",
        type=int,
        default=100,
    )
    parser.add_argument(
        "-g", "--generations",
        help="Quantidade de gerações em cada realização.",
        type=int,
        default=800,
    )
    args = parser.parse_args()

    tw = textwrap.TextWrapper(subsequent_indent='      ')

    if  args.open is not None  and  args.title is not None:
        msg = (
            "Escolha apenas uma das opções: ou executa o algoritmo (e"
            " salva os resultados na shelve) ou abre a shelve usando a"
            " flag '-o'."
        )
        print(tw.fill("ERRO: " + msg), file=sys.stderr)
        sys.exit(1)

    if args.open is None:
        main(args.runs, args.generations, shelve_title=args.title)
        sys.exit(0)

    # Com certeza args.title é None, e args.open não é None
    filename = args.open
    if filename is Ellipsis:
        print(tw.fill(
            "Buscando última shelve usada. O nome dela estará"
            " disponível na variável ‘filename’."
        ))
        filename = shelve_helper.get_last_shelve()
    if not os.path.isfile(filename):
        # Tratar args.open como título
        candidates = shelve_helper.get_shelves(args.open)
        if len(candidates) == 0:
            msg = "Não existe uma shelve com título ‘{}’.".format(
                args.open)
            print(tw.fill("ERRO: " + msg), file=sys.stderr)
            sys.exit(2)
        elif len(candidates) > 1:
            msg = "Existe mais de uma shelve com título ‘{}’:".format(
                args.open)
            print(tw.fill("ERRO: " + msg), file=sys.stderr)
            for c in candidates:
                print('  > ' + c, file=sys.stderr)
            sys.exit(3)
        # here, len == 1
        filename = candidates[0]
        del candidates
    with shelve.open(filename) as shv:
        for k in shv:
            globals()[k] = shv[k]
    del parser, k, tw, args
    os.environ['PYTHONINSPECT'] = '1'
