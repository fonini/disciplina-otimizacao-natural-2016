#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Otimização Natural - 2016.1

Exercício 1
"""

import math

import numpy as np
import matplotlib.pyplot as plt

import EC
import GA

num_bits = 16

IndivíduoSGA, PopulaçãoSGA = GA.SimpleGeneticAlgorithm(num_bits)

class IndivíduoEx1(IndivíduoSGA):
    def phenotype(self):
        b = EC.bin_from_gray(self.chromossome)
        u = sum(2**k for k in range(num_bits) if b[k])
        u /= 2**num_bits
        return -2 + 4*u
    def update_fitness(self):
        ph = self.phenotype()
        self.fitness = -ph**2 + 0.3*math.cos(10*math.pi*ph)


x = np.arange(-2.0, 2.0, 0.001)
y = x**2 - 0.3*np.cos(10*np.pi*x)

pop = PopulaçãoSGA(20, IndivíduoEx1)


plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=20)

for k in range(0, 100, 30):

    pop.evolve(lambda p: p.generation >= k)

    plt.plot(x,y)
    plt.plot(*zip(*((m.phenotype(), -m.fitness) for m in pop.members)),
             linestyle='None', marker='x', markersize=11, markeredgewidth=3)
    plt.xlabel('$x$')
    plt.ylabel('Fitness')
    plt.title('After ${}$ iterations'.format(pop.generation))
    plt.grid(True)
    plt.show()

print(pop)
print(pop.best_genotype.phenotype())
