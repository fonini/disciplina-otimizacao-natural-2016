%
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
%

x0 = randn;
N = 1e6;
J = @(x) x.^2;
R = @(x) x + 0.1*( 2*rand-1 );
T = 0.1;

[X, js, x_min, J_min] = metropolis(x0, N, J, R, T);
fprintf('estado de menor energia: x_min = %x\n', x_min);
fprintf('menor energia atingida:  J_min = %x\n', J_min);

%
% Plot temporal de X(n)
%
figure(1);
plot(X);
grid;

%
% Histograma das últimas amostras de X(n)
%
figure(2);
last_n = N/10;
last_X = X(end-last_n+1:end);
limits_x = [-1,1];
bins = 100;
dx = diff(limits_x)/bins;
t = (limits_x(1)+dx/2):dx:(limits_x(2)-dx/2);
hist(last_X, t);
hold on;
sigma_sq = T/2;
qtdes_x = last_n * dx * (1/sqrt(2*pi*sigma_sq)) * exp(-t.^2/(2*sigma_sq));
plot(t, qtdes_x, 'r');

%
% Histograma das energias das últimas amostras de X(n)
%
figure(3);
limits_j = [0,1];
bins = 100;
dj = diff(limits_j)/bins;
t = (limits_j(1)+dj/2):dj:(limits_j(2)-dj/2);
hist(js(end-last_n+1), t);
hold on;
qtdes_j = last_n * dj * (1./sqrt(2*pi*sigma_sq*t)) .* exp(-t/(2*sigma_sq));
% Aproximação linear em dx não funciona na primeira bin:
qtdes_j(1) = last_n * erf(sqrt(dj/(2*sigma_sq))); % linear em sqrt(dx)
plot(t, qtdes_j, 'r');

%
% Plot mostrando a convergência das distribuições
%
bins = 100;
dx = diff(limits_x)/bins;
t = (limits_x(1)+dx/2):dx:(limits_x(2)-dx/2);
% Para demorar menos, aumente a resolução
resolucao = 1000;
err = zeros(1, N/resolucao);
w = waitbar(0, 'Preparando gráfico de convergência...');
for k=1:(N/resolucao)
    q = resolucao*k;
    pdf = (1/sqrt(2*pi*sigma_sq)) * exp(-t.^2/(2*sigma_sq));
    h = hist(X(1:q), t);
    err(k) = norm(pdf - h/(q*dx));
    waitbar(k/(N/resolucao),w);
end
delete(w);
figure(4);
loglog(resolucao*(1:(N/resolucao)), err/sqrt(dx));
grid;
