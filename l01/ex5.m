%
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
%

P = 4; % número de partículas
J = @J_ex5;
R = @(x) x + 0.1*randn(size(x));

T0 = 0.5;
N = 1e5; % número de iterações dentro de cada metropolis
M = 31; % número de realizações do metropolis
T_plot = [1 5 31];
T = T0 ./ log2(1+(1:M));

x0 = zeros(P,1);

[x_plot, js_plot, x_min_plot, x_min, J_min] = ...
    simanneal(x0, N, J, R, T, T_plot);

L=N/10;
limits = [0,15];
bins = 150;
dj = diff(limits)/bins;
t = (limits(1)+dj/2):dj:(limits(2)-dj/2);

T = T(T_plot);
for k = 1:length(T_plot);
    figure;
    hist(js_plot((N-L+1):N, k),t);
    xlabel('Custo');
    title(sprintf('T = %f  --  custo médio = %f', T(k), mean(js_plot(:,k))));

    figure;
    x = -.25:.001:3.25;
    plot(x, (x.*(x-1).*(x-2).*(x-3)).^2);
    grid;
    hold on;
    x_typ = x_plot{k}(:,end);
    plot(x_typ, zeros(size(x_typ)), 'ro');
    title(sprintf('T = %f  --  estado típico  --  custo = %f', ...
          T(k), J(x_typ)));

    figure;
    plot(x, (x.*(x-1).*(x-2).*(x-3)).^2);
    grid;
    hold on;
    x_m = x_min_plot(:,k);
    plot(x_m, zeros(size(x_m)), 'ro');
    title(sprintf('T = %f  --  estado barato  --  custo = %f', ...
          T(k), J(x_m)));

    figure;
    plot(x_plot{k}');
    title(sprintf('T = %f', T(k)));
    grid;
end
