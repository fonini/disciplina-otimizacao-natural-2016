%
% Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
%

J = @(x) -x + 100 * (x-.2).^2 * (x-.8).^2;
R = @(x) x + 0.1*randn(size(x));

T0 = 1;
N = 1e5; % número de iterações dentro de cada metropolis
M = 20; % número de realizações do metropolis
T = T0 ./ log2(1+(1:M));
T_plot = [1 6 12 20];

x0 = 0;
x_plot  = cell(1, length(T_plot));
js_plot = zeros(N, length(T_plot));
x_min_plot = zeros(size(x0,1), length(T_plot));

w = waitbar(0);
x_min = [];
J_min = Inf;
k_plot = 1;
for k=1:length(T);
    waitbar((k-1)/length(T), w, sprintf('Temperatura #%d: %f', k-1, T(k)));
    [X, js, this_x_min, this_J_min] = metropolis(x0, N, J, R, T(k));
    waitbar(k/length(T), w, sprintf('Temperatura #%d: %f', k-1, T(k)));
    if this_J_min < J_min
        x_min = this_x_min;
        J_min = this_J_min;
    end
    x0 = X(:,end);
    if find(T_plot == k)
        x_plot{k_plot} = X;
        js_plot(:,k_plot) = js';
        x_min_plot(:,k_plot) = x_min;
        k_plot = k_plot + 1;
    end
end;
delete(w);

L=N/10;
limits = [-1,6];
bins = 140;
dj = diff(limits)/bins;
t = (limits(1)+dj/2):dj:(limits(2)-dj/2);

T = T(T_plot);
for k = 1:length(T_plot);
    figure;
    hist(js_plot((N-L+1):N, k),t);
    xlabel('Custo');
    title(sprintf('T = %f  --  custo médio = %f', ...
          T(k), mean(js_plot((N-L+1):N,k))));

    figure;
    plot(x_plot{k});
    title(sprintf('T = %f', T(k)));
    grid;

    fprintf('T = %f  --   custo ótimo: %.16f\n', T(k), J(x_min_plot(:,k)));
end
