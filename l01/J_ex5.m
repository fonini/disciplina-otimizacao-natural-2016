function J = J_ex5(x)

P = length(x);
J = sum((x.*(x-1).*(x-2).*(x-3)).^2);
for i=1:P,
    for j=(i+1):P,
        n = abs(x(i) - x(j));
        if n < 0.5
            J = J + (1 - 2*n);
        end
    end;
end;
