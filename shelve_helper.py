#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>

Shelve management utilities
"""

import os
import os.path
import glob

class GlobTimestamp:
    def __init__(self, string):
        self.string = string
    def __repr__(self):
        return "GlobTimestamp({!r})".format(self.string)
    def glob(self):
        result = ''
        it = iter(self.string)
        while True:
            try:
                c = next(it)
            except StopIteration:
                break
            if c == '%':
                try:
                    c = next(it)
                except StopIteration:
                    raise ValueError('GlobTimestamp: cannot end in ‘%’.')
                if   c == 'w': result += '[0-6]'
                elif c == 'd': result += '[0-3][0-9]'
                elif c == 'b': result += '[A-Z][a-z][a-z]'
                elif c == 'd': result += '[0-3][0-9]'
                elif c == 'm': result += '[0-1][0-9]'
                elif c == 'y': result += '[0-9][0-9]'
                elif c == 'Y': result += '[0-9][0-9][0-9][0-9]'
                elif c == 'H': result += '[0-2][0-9]'
                elif c == 'I': result += '[0-1][0-9]'
                elif c == 'M': result += '[0-5][0-9]'
                elif c == 'S': result += '[0-5][0-9]'
                elif c == 'f': result += '[0-9]'*6
                elif c == 'j': result += '[0-3][0-9][0-9]'
                elif c == 'U': result += '[0-5][0-9]'
                elif c == 'W': result += '[0-5][0-9]'
                elif c == '%': result += '%'
                else:
                    raise ValueError(
                        'GlobTimestamp: unknown directive ‘%{}’'.format(c))
            else:
                result += glob.escape(c)
        return result

globts = GlobTimestamp("%Y-%m-%d_%H:%M:%S.%f")

class GetShelveError(LookupError):
    pass

def get_shelves(title=None):
    prefix = 'shelve__'
    postfix = ''
    if title is not None:
        postfix = '__' + title
    glob_expr = prefix + globts.glob() + postfix
    return glob.glob(glob_expr)

def get_last_shelve(*args, **kwargs):
    most_recent_candidate = None
    most_recent_time = 0
    for f in os.listdir(*args, **kwargs):
        # Test if is shelve
        if not f.startswith("shelve__"):
            continue
        # Test if is recent
        this_time = 0
        try:
            this_time = os.path.getmtime(f)
        except OSError:
            pass
        else:
            if this_time > most_recent_time:
                most_recent_time = this_time
                most_recent_candidate = f
    if most_recent_candidate is not None:
        return most_recent_candidate
    else:
        raise GetShelveError(
            "There are no shelves in the current directory.")
